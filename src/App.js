import React from 'react';
import { Container} from '@material-ui/core';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import PostDetails from './components/PostDetails/PostDetails';
import Home from './components/Home/Home';
import Navbar from './components/Navbar/Navbar';
import Auth from './components/Auth/Auth';
import Form from './components/Form/Form';
import ActivationEmail from './components/Auth/ActivationEmail'; 
import {Business} from './components/Business/Business';
import Creation from './components/Creation/creation';
import CreatorPostDetails from './components/CreatorPostDetails/PostDetails';
import {Activate} from './components/Auth/Activate.js';

const App = () => {
  const user = JSON.parse(localStorage.getItem('profile'));
  
  return (
        <BrowserRouter>
        <Navbar />
        
          <Container maxWidth="xl">
            <Switch>
              <Route path="/" exact component={() => <Redirect to="/posts" />} />
              <Route path="/posts" exact component={Home} />
              <Route path="/posts/search" exact component={Home} />
              <Route path="/posts/:id" component={PostDetails} />
              <Route path="/auth" exact component={() => (!user ? <Auth /> : <Redirect to="/posts"/>)} />
              <Route path="/creationhistory" exact component={Creation} />
              <Route path="/creationhistory/search" exact component={Creation} />
              <Route path="/creationhistory/:id" component={CreatorPostDetails} />
              <Route path="/creators" exact component={Form}/>
              <Route path="/business" exact component={Business} />
              <Route path="/user/activate/:activation_token" exact component={ActivationEmail} />
              <Route path="/activate" exact component={Activate} />
            </Switch>
          </Container>
        </BrowserRouter>
  );   
};
export default App;
