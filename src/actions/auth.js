import { AUTH, LOGOUT } from '../constants/actionTypes';
import * as api from '../api/index.js';

export const signin = (formData, history) => async (dispatch) => { // tut used state instead of history
  try {
    const { data } = await api.signIn(formData); //log in user

    dispatch({ type: AUTH, data }); 

    history.push('/creators'); //at this state he had history instead of history 
  } catch (error) {
    console.log(error);
  }
};

export const signup = (formData, history) => async (dispatch) => { // tut used history instead of history
  try {
    const { data } = await api.signUp(formData); //sign up user

    dispatch({ type: AUTH, data });
    dispatch({ type: LOGOUT });

    history.push('/activate');  //at this state he had history instead of history 
  } catch (error) {
    console.log(error);
  }
};