import React, {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import axios from 'axios'
import { Typography, Paper } from '@material-ui/core';
import useStyles from './styles';
//import { activateEmail } from '../../../../server/controllers/user';
//import {showErrMsg, showSuccessMsg} from '../../utils/notification/Notification'


const ActivationEmail = () => {
    const {activation_token} = useParams()
    const [err, setErr] = useState('')
    const [success, setSuccess] = useState('')
    const classes = useStyles();


    useEffect(() => {
        console.log(activation_token)
        if(activation_token){
            const activationEmail = async () => {
                try {
                    const res = await axios.get('http://localhost:5000/user/activate/'+activation_token)   //Todo: URL anpassen 
                    setSuccess(res.data.msg)
                } catch (err) {
                    err.response.data.msg && setErr(err.response.data.msg)
                }
            }
            activationEmail()
        }
    },[activation_token])

    return ( 
        <Paper className={classes.paper}>
        <Typography variant="h6" align="center">
          Thank you for activating your account. Please sign in to transcribe your podcasts.
        </Typography>
      </Paper>

      
    )
};

export default ActivationEmail;
