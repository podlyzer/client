import { START_LOADING, END_LOADING, FETCH_ALL, FETCH_POST, FETCH_BY_SORT_FILTER, FETCH_BY_SEARCH, FETCH_CREATOR_BY_SEARCH ,FETCH_CREATORALL, FETCH_CREATORPOST, CREATE, UPDATE, LIKE } from '../constants/actionTypes';
import * as api from '../api/index.js';

export const getPost = (id) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });

    const { data } = await api.fetchPost(id);

    dispatch({ type: FETCH_POST, payload: { post: data } });
  } catch (error) {
    console.log(error);
  }
};

export const getPosts = ( page) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data, currentPage, numberOfPages } } = await api.fetchPosts( page);

    dispatch({ type: FETCH_ALL, payload: { data, currentPage, numberOfPages } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};
/* export const getPostsBySortFilter = (sort, sortDirection, category) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data } } = await api.fetchPostsBySortFilter(sort, sortDirection, category);

    dispatch({ type: FETCH_BY_SORT_FILTER, payload: { data } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
}; */

export const getPostsBySearch = (searchQuery) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data } } = await api.fetchPostsBySearch(searchQuery);

    dispatch({ type: FETCH_BY_SEARCH, payload: { data } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};

export const getCreatorsPostsBySearch = (searchQuery) => async (dispatch) => {
  try {
    dispatch({ type: START_LOADING });
    const { data: { data } } = await api.fetchCreatorsPostsBySearch(searchQuery);

    dispatch({ type: FETCH_CREATOR_BY_SEARCH, payload: { data } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};


export const getCreatorPost = (id) => async (dispatch) => {
    try {
    dispatch({ type: START_LOADING });

    const { data } = await api.fetchCreatorPost(id);

    dispatch({ type: FETCH_CREATORPOST, payload: data });
    dispatch({type: END_LOADING});
  } catch (error) {
    console.log(error);
  }
}; 

export const getCreatorPosts = (page) => async (dispatch) => {
    try {
    dispatch({ type: START_LOADING });
    const { data: { data, currentPage, numberOfPages } } = await api.fetchCreatorPosts(page);

    dispatch({ type: FETCH_CREATORALL, payload: { data, currentPage, numberOfPages } });
    dispatch({ type: END_LOADING });
  } catch (error) {
    console.log(error);
  }
};

export const createPost = (post, history) => async (dispatch) => {
  try {
    //dispatch({ type: START_LOADING });
      
    history.push('/creationhistory');

    const { data } = await api.createPost(post);

    dispatch({ type: CREATE, payload: data });

    //history.push(`/posts/${data._id}`);
  } catch (error) {
    console.log(error);
  }
};

export const updatePost = (id, post) => async (dispatch) => {
  try {
    const { data } = await api.updatePost(id, post);

    dispatch({ type: UPDATE, payload: data });
  } catch (error) {
    console.log(error);
  }
};

export const likePost = (id) => async (dispatch) => {
  const user = JSON.parse(localStorage.getItem('profile'));

  try {
    const { data } = await api.likePost(id, user?.token);

    dispatch({ type: LIKE, payload: data });
  } catch (error) {
    console.log(error);
  }
};