import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    appBarSearch: {
        marginBottom: '1rem',
        padding: '16px', 
        width: '100%',
        position: 'relative',
        display: 'flex',
    },

    
    searchWrap: {
        width: '75%',
        position: 'relative',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%)',
    },

    pagination: {
        borderRadius: 4,
        marginTop: '1rem',
        padding: '16px',
    },

    
    searchTerm: {
        width: '85%',
        border: '3px solid #f2994a',
        borderRight: 'none',
        padding: '5px',
        height: '30px',
        borderRadius: '5px 0 0 5px',
        outline: 'none',
        color: '#e354f7',
        fontSize:'20px',
    },
   

    searchButton: {
        width: '15%',
        height: '46px',
        border: '1px solid #f2994a',
        background: '#f2994a',
        textAlign: 'center',
        color: '#fff',
        borderRadius: '0 5px 5px 0',
        cursor: 'pointer',
        fontSize: '20px',
    },
}));