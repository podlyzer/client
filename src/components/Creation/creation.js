import React, { useState, } from 'react';
import { Container, Grow, Grid, Paper } from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import "./style.scss";

import { getCreatorsPostsBySearch } from '../../actions/posts';
import Posts from '../CreatorPosts/Posts';
import Pagination from '../CreatorPagination';
import useStyles from './styles';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const Creation = () => {

  const classes = useStyles();
  const query = useQuery();
  const page = query.get('page') || 1;
  const searchQuery = query.get('searchQuery');

  const [currentId, setCurrentId] = useState(0);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
  const dispatch = useDispatch();

  const [search, setSearch] = useState('');
  const [keywords, setKeywords] = useState([]);
  const history = useHistory();


  if (!user?.result?.name) {
      history.push('/auth');
    }

  const searchCreatorsPost = () => {
      if (search.trim() || keywords) {
        dispatch(getCreatorsPostsBySearch({ search, keywords: keywords.join(',').toLowerCase() }));
        history.push(`/creationhistory/search?searchQuery=${search || 'none'}`);
      } else {
        history.push('/');
      }
    };

    const handleKeyPress = (e) => {
      if (e.keyCode === 13) {
        searchCreatorsPost();
      }
    };
  

  return (
    <Grow in>
      <Container maxWidth="xl">

        {/* Search Bar */}
        <div item class="container" >
          <div class="form">
            <div class="finder">
              <div class="finder__outer">
                <div class="finder__inner">
                  <i class="fas fa-search fa-lg"></i>
                  <input class="finder__input"  type="text" name="search" variant="outlined" placeholder="Search for title or keywords" fullWidth value={search} onKeyDown={handleKeyPress} onChange={(e) => setSearch(e.target.value)} />
                </div>
              </div>
            </div>
          </div>
        </div>

        <Grid container justify="space-between" alignItems="stretch" spacing={3} >

        <div  class ="paper"alignItems="centre">
           <p>Hover over the tile to see the podcast you have created and click on it to see the transcript, it might take some time for the transcribing process. The podcast transcript will also be emailed to you after the process is completed!</p>
        </div>






          {/* Posts*/}
          <Grid >
            <Posts setCurrentId={setCurrentId} />
          </Grid>


          <Grid item xs={12} sm={6} md={12}></Grid>

          <Grid item xs={9} sm={9} md={5}>
            {/* Posts*/}
            {(!searchQuery && !keywords.length) && (
              <Paper className={classes.pagination} elevation={6}  >
                <Pagination page={page} />
              </Paper>
            )}
          </Grid>

        </Grid>
      </Container>
    </Grow>
  );
};

export default Creation;