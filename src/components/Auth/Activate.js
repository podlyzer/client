import React from 'react';
import { Typography, Paper } from '@material-ui/core';
import useStyles from './styles';

export const Activate = () => {
    const classes = useStyles();

    return (
      <Paper className={classes.paper}>
        <Typography variant="h6" align="center">
          Thank you for signing up. Please check your emails to verify your account.
        </Typography>
      </Paper>
    )
};