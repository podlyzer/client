import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useHistory } from "react-router-dom";
import "./style.scss";
import moment from "moment";
import { Container, ButtonBase, Paper, CircularProgress } from "@material-ui/core";

import { getCreatorPost } from "../../actions/posts";
import useStyles from "./styles";

//CodePen

const Post = () => {
  const { post, posts, isLoading } = useSelector((state) => state.posts);
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const { id } = useParams();

  useEffect(() => {
    dispatch(getCreatorPost(id));
  }, [id]);

  if (!post) return null;
  

  if (isLoading) {
    return (
      <Paper elevation={6} className={classes.loadingPaper}>
        <CircularProgress size="7em" />
      </Paper>
    );
  }

  return (
    <Container> 
    <section class="showcase">
      <div class="text">
        <h4 class="logo">{post.podcastTitle}</h4>
        <h3>{post.episodeTitle}</h3>
        <h6 class="category">Category:  {post.category}</h6>
        {/* <h5 class="keywords">#{post.keywords}</h5> */}
        <i>{moment(post.createdAt).fromNow()}</i>
        <p>{post.description}</p>
        <h6 class="category">Listen to Podcast</h6>
        <audio
        controls src={post.podlink}>
            Your browser does not support the
            <code>audio</code> element.
        </audio>
        <a class="info" href={post.link}>
        <div class="link">Check out more if audio is not available</div>
        </a>
        {/* <ButtonBase  onClick={openPost}>
             <p>{post.link}</p>
        </ButtonBase> */}
      </div>

      <div class="img">
        <img class="media" src={post.selectedFile} />
      </div>

    </section>
    <section class="showcase">
        <div class="info ">
          <p class="title">Keywords</p>
          <p class="keywords">#{post.keywords}</p>
          <p class="title">Transcript</p>
          {post.transcript
            .replace(/([.?!])\s*(?=[A-Z])/g, "$1 |")
            .split("|")
            .map(i => {
            return <p class="text">{i}</p>
            })}
            <p></p>
          
          
        </div>
    </section>
    </Container>

  );
};

export default Post;