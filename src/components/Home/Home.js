import React, { useState, } from 'react';
import { Container, Grow, Grid, Paper } from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';


import { getPostsBySearch } from '../../actions/posts';
import Posts from '../Posts/Posts';
import Pagination from '../Pagination';
import useStyles from './styles';


function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const Home = () => {

  const classes = useStyles();
  const query = useQuery();
  const page = query.get('page') || 1;
  const searchQuery = query.get('searchQuery');
  /* const sort = query.get('sort');
  const sortDirection = query.get('1');
  const category = query.get('category'); */

  const [currentId, setCurrentId] = useState(0);
  const dispatch = useDispatch();

  const [search, setSearch] = useState('');
  const [keywords, setKeywords] = useState([]);
  const history = useHistory();


  const searchPost = () => {
    if (search.trim() || keywords) {
      dispatch(getPostsBySearch({ search, keywords: keywords.join(',').toLowerCase() }));
      history.push(`/posts/search?searchQuery=${search || 'none'}`);
    } else {
      history.push('/');
    }
  };
  
  /* const sortFilterPost = () => {
    if (sort, sortDirection, category){
      dispatch(getPostsBySortFilter({sort, sortDirection, category}));
      history.push(`/posts/sort=${sort}&sortDirection=${sortDirection}`);
    }
    else {
      history.push('/');
    }
  };
  
  const handleKeyPressed = (e) => {
    if (e.keyCode === 13) {
      sortFilterPost();
    }
  }; */

  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      searchPost();
    }
  };


  return (
    <Grow in>
      <Container maxWidth="xl">

        {/* Search Bar */}
        <div item class="container" >
          <div class="form">
            <div class="finder">
              <div class="finder__outer">
                <div class="finder__inner">
                  <i class="fas fa-search fa-lg"></i>
                  <input class="finder__input"  type="text" name="search" variant="outlined" placeholder="Search for title or keywords" fullWidth value={search} onKeyDown={handleKeyPress} onChange={(e) => setSearch(e.target.value)} />
                </div>
              </div>
            </div>
          </div>
        </div>

        <Grid container spacing={2}>

         

          {/* Posts*/}
          <Grid item xs >
            <Posts setCurrentId={setCurrentId} />
          </Grid>

        </Grid>

        <Grid container spacing={6}>
          <Grid item xs></Grid>
          <Grid item xs={6}>
            {(!searchQuery && !keywords.length) && (
              <Paper className={classes.pagination} elevation={6}  >
                <Pagination page={page} />
              </Paper>
            )}
          </Grid>
          <Grid item xs></Grid>
        </Grid>




      </Container>
    </Grow >
  );
};

export default Home;