import React from 'react';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import './style.scss';
import { useHistory } from 'react-router-dom';
import {ButtonBase} from '@material-ui/core';
import useStyles from './styles';

const Post = ({ post, setCurrentId }) => {
  const history = useHistory();
  const classes = useStyles();

  const openPost = () => {
    history.push(`/posts/${post._id}`);
  };

  class BlogCard extends React.Component {
    constructor(props) {
      super(props);
      this.state = { flipped: false };
      this.flip = this.flip.bind(this);
    }

    flip = () => {
      this.setState({ flipped: !this.state.flipped });
    }
    render() {
      return (
        <div onMouseEnter={this.flip} onMouseLeave={this.flip} className={"card-container" + (this.state.flipped ? " flipped" : "")}>
          <Front />
          <Back />
        </div>
      )
    }
  }

  class Front extends React.Component {
    render() {
      return (
        <div className="front">
          
          <ImageArea />
          <MainArea />
          
        </div>
      )
    }
  }

  class Back extends React.Component {
    render() {
      return (
        
        <div className="back">
          <ButtonBase /*component="span" name="test" */ className={classes.cardAction} onClick={openPost}>
          <h2 className="head">Details:</h2>
          <div></div>
          <div></div>
            <p>{post.description}</p>
          </ButtonBase>
        </div>
        
      )
    }
  }

  class ImageArea extends React.Component {
    render() {
      return (
        <div className="image-container">
          <img className="card-image" src={post.selectedFile}></img>
          <h1 className="title">{post.episodeTitle }</h1>

        </div>
      )
    }
  }

  class MainArea extends React.Component {
    render() {
      return (
        <div className="main-area">
          <div className="blog-post">
            
            <p className="date">{moment(post.createdAt).format('DD-MM-YYYY')}</p>
            <p className="blog-content">{post.description}</p>
            <p className="read-more">Hover to read more...</p>
          </div>
        </div>
      )
    }
  }

  return (
    <div className="page-container">
      <div><BlogCard /></div>
    </div>
  );
};
export default Post;

// right now the name of the podcast is not displayed. If this will be the case in the future we have to keep in mind that the creator is automatically added from the profile as name (tut3 2:19)