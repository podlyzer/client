import React from 'react';
import { Grid, CircularProgress } from '@material-ui/core';
import { useSelector } from 'react-redux';

import Post from './Post/Post';
import useStyles from './styles';

const Posts = ({ post,setCurrentId}) => {

  const { posts, isLoading } = useSelector((state) => state.posts);
  const classes = useStyles();

  if (!posts.length && !isLoading) return 'No posts';  //tut2:24:07

  return (
    isLoading ? <CircularProgress /> : (

      <Grid container alignItems="center" spacing={2}>
        {posts?.map((post) => (
          <Grid key={post._id} item >
            <Post post={post} setCurrentId={setCurrentId} />
          </Grid>

        ))}
      </Grid>
    )
  );
};

export default Posts;
