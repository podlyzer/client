import React, { useState, useEffect } from 'react';
import { AppBar, Typography, Toolbar, Avatar, Button } from '@material-ui/core';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import decode from 'jwt-decode';

import * as actionType from '../../constants/actionTypes';
import useStyles from './styles';
import './style.scss';




const Navbar = () => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));
  const dispatch = useDispatch();
  const location = useLocation();
  const history = useHistory();
  const classes = useStyles();


  const logout = () => {
    dispatch({ type: actionType.LOGOUT });

    history.push('/');

    setUser(null);
  };

  useEffect(() => {
    const token = user?.token;

    if (token) {
      const decodedToken = decode(token);

      if (decodedToken.exp * 1000 < new Date().getTime()) logout();
    }

    setUser(JSON.parse(localStorage.getItem('profile')));
  }, [location]);




  return (


    <nav class="navbar navbar-expand-custom navbar-mainbg ">

      <a class="navbar-brand navbar-logo " href="/posts">Podlyzer</a>


      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <div class="hori-selector">
            <div class="left"></div>
            <div class="right"></div>
          </div>
          <li class="nav-item ">
            <a href="/posts" class="nav-link" ><i class="fas fa-headphones"></i>Home</a>
          </li>
          <li class="nav-item">
            <a href="/creators" class="nav-link" ><i class="far fa-file-alt"></i>For Creators</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="/business"><i class="fas fa-business-time"></i>For Businesses</a>
          </li>


          {user ? (
            <div >
              <li class="nav-item">
                <a href="/creationhistory" class="nav-link" ><i class="fas fa-atlas"></i>Creation History</a>
              </li>


              <div class="user-settings">

                <div class="user-menu">
                  <svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-square"></svg>
                </div>

                <img class="user-profile" src={user.result.imageUrl} alt={user.result.name}></img>
                <p class="user-name" color="primary" >{user.result.name}</p>
                <div class="d-grid gap-2 d-md-block button-custom ">
                  <a type="button" class="btn btn-danger " onClick={logout} >Log Out</a>
                </div>
              </div>
            </div>
          ) : (
            <div class="d-grid gap-2 d-md-block button-custom-signIn ">
              <a type="button" class="btn btn-success" href="/auth" role="button" >Sign In</a>
            </div>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;

