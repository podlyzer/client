import React, { useState, useEffect } from 'react';
import { TextField, Button, Typography, Paper, Container } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import FileBase from 'react-file-base64';

import { useHistory } from 'react-router-dom';
import useStyles from './styles';
import { createPost, updatePost } from '../../actions/posts';
import "./style.scss";

//GET THE CURRENT ID

const Form = () => {
  const [currentId, setCurrentId] = useState(0);
  const [postData, setPostData] = useState({ podcastTitle: '', episodeTitle: '', description: '', link: '', keywords: [], selectedFile: '', podFile: '' });
  const post = useSelector((state) => (currentId ? state.posts.posts.find((postmessages) => postmessages._id === currentId) : 0));
  const dispatch = useDispatch();
  const classes = useStyles();
  const user = JSON.parse(localStorage.getItem('profile'));
  const history = useHistory();

  const clear = () => {
    setCurrentId(0);
    setPostData({ podcastTitle: '', episodeTitle: '', description: '', link: '', keywords: [], selectedFile: '', podFile: '' });
  };

  useEffect(() => {
    if (post) setPostData(post);
  }, [post]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (currentId === 0) {
      dispatch(createPost({ ...postData, name: user?.result?.name }, history));
      clear();
    } else {
      dispatch(updatePost(currentId, { ...postData, name: user?.result?.name }));
      clear();
    }
  };

  if (!user?.result?.activated && !user?.result?.googleId) {
    /*return (
      <Paper className={classes.paper}>
        <Typography variant="h6" align="center">
          Please Sign In to Upload Your Podcast for Transcription.
        </Typography>
      </Paper>*/
    history.push('/auth');

  };

  return (
    <Container componamenent="main" maxWidth="md">

      <Paper className={classes.paper} elevation={6}>
        <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
          <h1><strong>Transcribe</strong> <em>your</em> Podcast</h1>
          
        
          <TextField  name="podcastTitle" variant="outlined" label="Podcast Title" fullWidth value={postData.podcastTitle} onChange={(e) => setPostData({ ...postData, podcastTitle: e.target.value })} />
          <TextField name="episodeTitle" variant="outlined" label="Episode Title" fullWidth value={postData.episodeTitle} onChange={(e) => setPostData({ ...postData, episodeTitle: e.target.value })} />
          <TextField name="description" variant="outlined" label="Description" fullWidth multiline rows={4} value={postData.description} onChange={(e) => setPostData({ ...postData, description: e.target.value })} />
         <TextField name="category" variant="outlined" label="Category" fullWidth value={postData.category} onChange={(e) => setPostData({ ...postData, category: e.target.value })} />
          <TextField name="link" variant="outlined" label="Link" fullWidth value={postData.link} onChange={(e) => setPostData({ ...postData, link: e.target.value })} />
          <TextField name="keywords" variant="outlined" label="keywords (coma separated)" fullWidth value={postData.keywords} onChange={(e) => setPostData({ ...postData, keywords: e.target.value.split(',') })} />

        

           
          <h2><em>Upload your files</em> </h2>

          <div class="container">
            <div class="form-group" x-data="{ fileName: '' }">
              <div class="input-group shadow">
              <span class="input-group-text px-3 text-muted"><i class="fas fa-image fa-md"></i></span>
                <input type="text" class="form-control form-control-lg" placeholder="Upload Image" />
                <FileBase class="browse btn btn-primary px-4" type="file" multiple={false} onDone={({ base64 }) => setPostData({ ...postData, selectedFile: base64 })} />
                </div>
                <div class="input-group shadow"> 
                <span class="input-group-text px-3 text-muted"><i class="far fa-file-audio fa-lg"></i></span>
                <input type="text" class="form-control form-control-lg" placeholder="Upload Audio" />
                <FileBase type="file" multiple={false} onDone={({ base64 }) => setPostData({ ...postData, podFile: base64 })} />
                </div>
            </div>
          </div>


          <Typography variant="caption">Clicking "Submit" implies you fully own this Podcast, Podlyzer has no legal responsibility related to it.</Typography>

          <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>Submit</Button>
          <Button variant="contained" color="secondary" size="small" onClick={clear} fullWidth>Clear</Button>
        </form>
      </Paper>
    </Container>
  );
};

export default Form;